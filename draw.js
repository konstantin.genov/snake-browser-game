const canvas = document.querySelector(".canvas");
const context = canvas.getContext("2d");
const scale = 20;

const rows = canvas.height / scale;
const columns = canvas.height / scale;

console.log("%c Log:", "color: red; font-weight: bold");
console.log({ canvas });
console.trace();

let snake; // KING OF SNEK

// IIFE
(function setup() {
  snake = new Snake();
  fruit = new Fruit();
  fruit.pickLocation();

  window.setInterval(() => {
    context.clearRect(0, 0, canvas.width, canvas.height);
    fruit.draw();
    snake.update();
    snake.draw();

    if (snake.eat(fruit)) {
      fruit.pickLocation(); //when the snake touches the fruit, update location
      document.querySelector(".score").innerText = snake.eaten;
    }
  }, 250);
})();

window.addEventListener("keydown", evt => {
  const direction = evt.key.replace("Arrow", "");
  snake.changeDirection(direction);
});
