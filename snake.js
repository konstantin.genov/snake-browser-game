// TODO: Add collision functionality to end the game when you bite your ass

class Snake {
  constructor() {
    this.x = 0; //cords
    this.y = 0;
    this.xSpeed = scale * 1; // x coordinate movement speed
    this.ySpeed = scale * 1; // y coordinate movement speed
    this.eaten = 0; // number of times the snake has eaten a fruit
    this.tail = [];
  }

  draw() {
    context.fillStyle = "white";
    for (let index = 0; index < this.tail.length; index++) {
      // loop through each fruit eaten to get exact length of tail
      context.fillRect(this.tail[index].x, this.tail[index].y, scale, scale);
      console.log(this.tail[index]);
    }

    context.fillRect(this.x, this.y, scale, scale);
  }

  eat(fruit) {
    if (this.x === fruit.x && this.y === fruit.y) {
      this.eaten++; // increment eaten fruit to adjust snake tail size
      return true;
    } else {
      return false;
    }
  }

  update() {
    for (let index = 0; index < this.tail.length - 1; index++) {
      this.tail[index] = this.tail[index + 1];
    }

    // draw the result from the loop
    this.tail[this.eaten - 1] = { x: this.x, y: this.y };

    //change snake's movement speed
    this.x += this.xSpeed;
    this.y += this.ySpeed;

    // adjust snake's position when it reaches a border
    if (this.x > canvas.width) {
      this.x = 0;
    }

    if (this.y > canvas.height) {
      this.y = 0;
    }

    if (this.x < 0) {
      this.x = canvas.width;
    }

    if (this.y < 0) {
      this.y = canvas.height;
    }
  }
  // handle key press logic
  changeDirection(direction) {
    switch (direction) {
      case "Up":
        this.xSpeed = 0;
        this.ySpeed = -scale * 1;
        break;
      case "Down":
        this.xSpeed = 0;
        this.ySpeed = scale * 1;
        break;
      case "Left":
        this.xSpeed = -scale * 1;
        this.ySpeed = 0;
        break;
      case "Right":
        this.xSpeed = scale * 1;
        this.ySpeed = 0;
        break;
    }
  }
}
